// console.log("Hello World");

// [SECTION] 	JSON OBJECT
/*
	-JSON stands for JavaScript Object Notation
	-A common use of JSON is to read data from web server , and display the data in a web page.
	-Syntax
	{
		"propertyA" : "valueA",
		"propertyB"	: "valueB"
 	}
*/

// JSON Objects
// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"country" : "Philippines"
// }

// // [SECTION] JSON Arry
// "cities":[
// {"city" : "Quezon City",  "province" : "Metro Manila","country" : "Philippines"} 
// {"city" : "Pasig City", "province" : "Metro Manila", "country" : "Philippines"} 
// {"city" : "Navotas City", "province" : "Metro Manila", "country" : "Philippines"} 
// ]

//[SECTION] JSON Methods
/*
	JSON.stringify - converts Javascript Object/Array into string
*/

let bactchesArr  = [
	{batchName: 'Batch 177'},
	{batchName: 'Batch 178'},
	{batchName: 'Batch 179'},	
];

console.log(bactchesArr);

let batchArrCont = JSON.stringify(bactchesArr);
console.log(`Result from stringify method:`);
console.log((bactchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
console.log(name);

//JSON.parse
// string to object
console.log(`Result from parse method:`);
let dataParse = JSON.parse(data)
console.log(dataParse);
console.log(dataParse.name);

console.log(JSON.parse(batchArrCont));